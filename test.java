/ 代码部门
//默认的输出目录...
  private static final String destSourcePath = "C:\\Users\\iiduka35\\Desktop\\labelme";

    public static void main(String[] args) {
        File destFile = new File(destSourcePath);
        if (!destFile.exists()) {
            destFile.mkdir();
        }
        try {
            //获取当前目录
            String nowPath = System.getProperty("user.dir");
            File nowFile = new File(nowPath);
            File[] txtFiles = nowFile.listFiles((dir, name) -> name.contains(".txt"));
            //获取到文件之后,保存名称
            String txtFilePath;
            String destFilePath;
            //批次该目录下的文件...
            for (File txtFile : txtFiles) {
                //处理这些文件
                txtFilePath = nowPath + "\\" + txtFile.getName();
                destFilePath = destSourcePath + "\\" + txtFile.getName();
                //内部方法在下面
               writeFile(txtFilePath,destFilePath);
            }
            System.out.println("success!!!");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    //这就是核心转换代码,commons-io里面的工具类...
    private static  void writeFile(String txtFilePath, String destFilePath) throws Exception {
    //由utf-8转gbk输出文本...
        FileUtils.writeLines(new File(destFilePath), "GBK", FileUtils.readLines(new File(txtFilePath),"UTF-8") );
    }
//记得添加pom的依赖
  <dependency>
        <groupId>commons-io</groupId>
        <artifactId>commons-io</artifactId>
        <version>2.4</version>
    </dependency>
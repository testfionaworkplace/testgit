# coding:utf-8
# encoding:utf-8
import os
import cv2
import numpy as np
import json
'''
1，数据堂，MBH 测试集  横版
2，生成带框图，（坐标文本dst),label,小图
3，label 及小图去掉竖版文本部分，坐标全部保留
4，去除\n,\t,\xa0
'''

def sort_poly(p):
    min_axis = np.argmin(np.sum(p, axis=1))
    p = p[[min_axis, (min_axis + 1) % 4, (min_axis + 2) % 4, (min_axis + 3) % 4]]
    if abs(p[0, 0] - p[1, 0]) > abs(p[0, 1] - p[1, 1]):
        return p
    else:
        return p[[0, 3, 2, 1]]


def imageAdjust(img, boxes):
    list_bbox = []
    list_rotate = []
    for box in boxes:
        if len(box) != 4:
            continue
        x, y, w, h = cv2.boundingRect(box)

        if x < 0:
            x = 0
        if y < 0:
            y = 0
        if w > img.shape[1]:
            w = img.shape[1]
        if h > img.shape[0]:
            h = img.shape[0]
        roi_bbox = img[y:y + h, x:x + w]
        rect = cv2.minAreaRect(box)
        newbox = np.int0(cv2.boxPoints(rect))
        angle = rect[2]
        if angle < -45:
            angle += 90
        center = (roi_bbox.shape[1] // 2, roi_bbox.shape[0] // 2)
        M = cv2.getRotationMatrix2D(center, angle, 1.0)
        M = M.astype(np.float32)
        pt1 = np.array([box[0][0] - x, box[0][1] - y, 1]).astype(np.float32)
        pt2 = np.array([box[1][0] - x, box[1][1] - y, 1]).astype(np.float32)
        pt3 = np.array([box[2][0] - x, box[2][1] - y, 1]).astype(np.float32)
        pt4 = np.array([box[3][0] - x, box[3][1] - y, 1]).astype(np.float32)
        rotate_pt1 = np.matmul(M, pt1)
        rotate_pt2 = np.matmul(M, pt2)
        rotate_pt3 = np.matmul(M, pt3)
        rotate_pt4 = np.matmul(M, pt4)

        r_box = np.array([rotate_pt1, rotate_pt2, rotate_pt3, rotate_pt4]).astype(np.int32)

        r_x, r_y, r_w, r_h = cv2.boundingRect(r_box)

        if r_x < 0:
            r_x = 0
        if r_y < 0:
            r_y = 0
        if r_w > roi_bbox.shape[1]:
            r_w = roi_bbox.shape[1]
        if r_h > roi_bbox.shape[0]:
            r_h = roi_bbox.shape[0]
        rotate_bbox = cv2.warpAffine(roi_bbox, M, (w, h))
        roi_rotate = rotate_bbox[r_y:r_y + r_h, r_x:r_x + r_w]
        list_bbox.append(roi_bbox)
        list_rotate.append(roi_rotate)
    return list_bbox, list_rotate


def load_json(txt_path, img_path, save_path, txt_save):
    det_coor = open(os.path.join(save_path, txt_path.split('\\')[-1].split('.')[0] + '.txt'), 'w', encoding='utf8')
    print(txt_path)
    img = cv2.imread(img_path)
    global total_count
    global error_row_count
    jsfile = open(txt_path, 'r', encoding='utf-8')
    jsf = json.load(jsfile)
    features = jsf['markResult']['features']
    boxes = []
    contents_many = []
    for line in features:
        coordinates = line['points']
        content=line['label']
        # print(coordinates)
        # print(content)
        # if 'TEXT' in content:
        #     text = content['TEXT']
        # else:
        #     text = '###'
        pointMat = np.asarray(coordinates).astype(np.float32).reshape((-1, 1, 2))
        rect = cv2.minAreaRect(pointMat)
        box = np.int0(cv2.boxPoints(rect))
        box = sort_poly(box.astype(np.int32))
        if np.linalg.norm(box[0] - box[1]) < 5 or np.linalg.norm(box[3] - box[0]) < 5:
            continue
        boxes.append(box)
        contents_many.append(content)
        det_coor.write('{},{},{},{},{},{},{},{}$$${}\n'.format(box[0, 0], box[0, 1], box[1, 0], box[1, 1], box[2, 0],box[2, 1], box[3, 0], box[3, 1], content))
        #cv2.polylines(img, [pointMat], True, color=(0, 255, 0), thickness=3)

    list_bbox, list_rotate = imageAdjust(img, boxes)
    for box in boxes:
        cv2.polylines(img, [box], True, color=(0, 255, 0), thickness=2)
        # cv2.imwrite(os.path.join(txt_save, txt_path.split('\\')[-1].split('.')[0] + '.jpg'), img)
    return list_rotate, contents_many

if __name__ == '__main__':
    # 图片路径
    img_root = 'C:\\Users\\iiduka35\\Desktop\\labelme\\img'
    # json路径
    json_root = 'C:\\Users\\iiduka35\\Desktop\\labelme\\json'
    # label以及文本行保存路径
    save_path = 'C:\\Users\\iiduka35\\Desktop\\labelme\\result\\rec'
    # 坐标信息，检测用
    dst_path = 'C:\\Users\\iiduka35\\Desktop\\labelme\\result\\dst'
    # 框图
    adj_path = 'C:\\Users\\iiduka35\\Desktop\\labelme\\result\\adjust'

    if not os.path.exists(save_path):
        os.makedirs(save_path)
    if not os.path.exists(dst_path):
        os.makedirs(dst_path)
    if not os.path.exists(adj_path):
        os.makedirs(adj_path)
    save_contents = open(os.path.join(save_path, 'labels.txt'), 'w', encoding='UTF-8')

    list_img = []
    list_txt = []
    DICT = {}
    for root2, dirs2, files2 in os.walk(json_root):
        for filename2 in files2:
            if filename2.find('.json') != -1:
                txt_path = os.path.join(root2, filename2)
                DICT[filename2.split('.json')[0]] = txt_path
                # list_txt.append(txt_path)
    for root, dirs, files in os.walk(img_root):
        for filename in files:
            if filename.find('.jpg') != -1:
                if filename.split('.jpg')[0] in DICT:
                    img_path = os.path.join(root, filename)
                    list_img.append(img_path)
                    list_txt.append(DICT[filename.split('.jpg')[0]])
    adjust_count = 0
    for i in range(len(list_img)):
        list_rotate, contents_many = load_json(list_txt[i], list_img[i], dst_path, adj_path)
        if len(list_rotate) != len(contents_many):
            break
        for j in range(len(list_rotate)):
            adjust_count += 1
            if (list_rotate[j].shape[0] * list_rotate[j].shape[1] >= 32 * 32) and (contents_many[j] != '###'):
                # if list_rotate[j].shape[1] < list_rotate[j].shape[0]*0.0001:
                #     #im_rot = np.rot90(list_rotate[j])
                #     print('111')
                #     #cv2.imwrite(os.path.join(save_path, list_img[i].split('\\')[-1].split('.jpg')[0] + '_' + str(j) + '.jpg'), im_rot)
                # else:
                    if list_rotate[j].shape[1] > list_rotate[j].shape[0] :    # 如果宽大于高
                       cv2.imwrite(os.path.join(save_path, list_img[i].split('\\')[-1].split('.jpg')[0] + '_' + str(j) + '.jpg'), list_rotate[j])
                       # contents_many[j]=contents_many[j].replace("\n", "").replace("\t", "").replace("\xa0", "").strip()
                       save_contents.write(list_img[i].split('\\')[-1].split('.jpg')[0] + '_' + str(j) + '.jpg' + '$$$' + contents_many[j].strip() + '\n')
